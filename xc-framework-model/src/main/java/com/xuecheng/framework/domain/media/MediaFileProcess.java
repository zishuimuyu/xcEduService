package com.xuecheng.framework.domain.media;

import lombok.Data;
import lombok.ToString;

/**
 *
 * @author：GJH
 * @createDate：2019/9/28
 * @company：洪荒宇宙加力蹲大学
 */
@Data
@ToString
public class MediaFileProcess {

    /**
     * 错误信息
     */
    private String errormsg;
}
